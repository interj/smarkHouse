
$(document).ready(function()
{
	setInterval(update_sensors, 1000);
});


function update_sensors()
{
    update_movement();
    update_gas();
    update_light();
    update_temp();
}

function update_movement()
{
    $.get( "movement/", function(data)
    {
		var value = $('#movement-sensor');
		value.html(data.sensorReading);
	}, "json");
}

function update_gas()
{
    $.get( "gases/", function(data)
    {
        var value = $('#gases-sensor');
        value.html(data.sensorReading);
	}, "json");

}

function update_light()
{
    $.get( "light/", function(data)
    {
        var value = $('#light-sensor');
        value.html('');
        $.each(data.sensorReadings, function(i, obj)
        {
            value.append(obj);
        });
    }, "json");
}

function update_temp()
{
    $.get( "temp/", function(data){
        var value = $('#temp-sensors');
        value.html('');
        $.each(data.sensorReadings, function(i, obj)
        {
            value.append(obj);
        });
    }, "json");
}

