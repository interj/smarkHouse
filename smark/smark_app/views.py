from django.shortcuts import render, render_to_response
from django.http import HttpResponse, Http404, HttpResponseRedirect, JsonResponse
from django.template import RequestContext, loader
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
import subprocess
import random

def home(request):
    return render(request, 'home.html')

def settings(request):

    return render(request, 'settings.html')

def index(request):
	return render(request, 'index.html')

def sensors(request):

	return render(request, 'sensors.html')


def get_movement(request):
    proc = subprocess.Popen(['/home/pi/scripts/test_move.sh'], stdout=subprocess.PIPE)
    line = proc.stdout.readline()

    if(line[0] == "1"):
        json = "Wykryto ruch!"
    else:
        json = "Brak ruchu."

    return JsonResponse({'sensorReading': json})

def get_gas(request):
    proc = subprocess.Popen(['/home/pi/scripts/test_gas.sh'], stdout=subprocess.PIPE)
    line = proc.stdout.readline()

    if(line[0] == "1"):
        json = "Wykryto latwopalne gazy!"
    else:
        json = "Powietrze jest czyste."

    return JsonResponse({'sensorReading': json})

def get_temperature(request):
    procs = []
    procs.append(subprocess.Popen(['sudo', 'cat', '/sys/bus/w1/devices/28-0000026177eb/w1_slave'], stdout=subprocess.PIPE))
    procs[0].stdout.readline()
    procs.append(subprocess.Popen(['sudo', 'cat', '/sys/bus/w1/devices/28-04146d6e35ff/w1_slave'], stdout=subprocess.PIPE))
    procs[1].stdout.readline()
    procs.append(subprocess.Popen(['sudo', 'cat', '/sys/bus/w1/devices/28-04146d768bff/w1_slave'], stdout=subprocess.PIPE))
    procs[2].stdout.readline()

    json = []
    json.append("Pokoj: " + str(float(procs[0].stdout.readline()[-6:])/1000))
    json.append("<br>Podloga: " + str(float(procs[1].stdout.readline()[-6:])/1000))
    json.append("<br>Na zewnatrz: " + str(float(procs[2].stdout.readline()[-6:])/1000))

    return JsonResponse({'sensorReadings': json})


def get_light(request):
    proc = subprocess.Popen(['sudo', '/home/pi/programs/i2c/light.py'], stdout=subprocess.PIPE)

    json = []
    for line in proc.stdout:
        json.append(line+ "<br>")
    json[-1] = json[-1][:-4]

    return JsonResponse({'sensorReadings': json})
