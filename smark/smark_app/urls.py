from django.conf.urls import url
from . import views

urlpatterns = [

    #url(r'^$', views.index),
    url(r'^sensors/$', views.sensors, name='sensors'),
    url(r'^sensors/movement/$', views.get_movement, name='movement'),
    url(r'^sensors/gases/$', views.get_gas, name='gases'),
    url(r'^sensors/temp/$', views.get_temperature, name='temperature'),
    url(r'^sensors/light/$', views.get_light, name='light'),
    url(r'^$', views.home, name='home'),
    url(r'^settings/$', views.settings, name='settings'),
]